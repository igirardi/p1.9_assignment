
Optimizations
========================

The following profiling has been executed with 108 atoms
and 10000 steps of size 5.
Profiling application before optimization improvements (with flags -O3 -pg):


Total time: 18.419s

.. |fig1| image:: Callgrind_step_0.png
   :scale: 100%
   :align: top

+---------+
| |fig1|  |
+---------+

Most of the elapsed time of the application is lost in
the calls of the function pow().
The results obtained with gprof are summarized below.

+----------------------------------------+
|  Each sample counts as 0.01 seconds    |
+-----+------------+----------+----------+
|  %  | cumulative |  self    |   name   |
+-----+------------+----------+----------+
|time |    seconds |       seconds       |      
+-----+------------+----------+----------+
|99.14|     3.44   |  3.44    |  force   |
+-----+------------+----------+----------+
|0.58 |     3.46   |  0.02    |   ekin   |
+-----+------------+----------+----------+
|0.58 |     3.48   |  0.02    |  sec_vel |
+-----+------------+----------+----------+
|0.00 |     3.48   |  0.00    |  azzero  |
+-----+------------+----------+----------+


Changing pow(x,6.0) and pow(x,12.0) to pow(x,6) and
pow(x,12) in the function force we observe a small
improvement from 3.44s to 3.40s.

Applying the 3rd Newton's law
________________________________

Applying the 3rd Newton's law the total execution time is:

Total time: 9.850s

The results with gprof are given below.


+----------------------------------------+
|  Each sample counts as 0.01 seconds    |
+-----+------------+----------+----------+
|  %  | cumulative |  self    |   name   |
+-----+------------+----------+----------+
|time |    seconds |       seconds       |      
+-----+------------+----------+----------+
|98.13|     1.83   |  1.83    |  force   |
+-----+------------+----------+----------+
|1.62 |     1.86   |  0.03    |   ekin   |
+-----+------------+----------+----------+
|0.54 |     1.87   |  0.01    |  sec_vel |
+-----+------------+----------+----------+
|0.00 |     1.87   |  0.00    |  azzero  |
+-----+------------+----------+----------+


We have obtained a speed-up of almost a factor of 2 (x1.87).


Avoid expensive operations pow(), sqrt(), division
________________________________________________________


Redefining the compution of the force in force.c
avoising the use of expensive math operations
reduces the total execution time to:

Total time: 1.562s

almost 12 times (x 11.79) faster than the version we tested so far.


The results with gprof are given below.


+----------------------------------------+
|  Each sample counts as 0.01 seconds    |
+-----+------------+----------+----------+
|  %  | cumulative |  self    |   name   |
+-----+------------+----------+----------+
|time |    seconds |       seconds       |      
+-----+------------+----------+----------+
|99.63|     1.48   |  1.48    |  force   |
+-----+------------+----------+----------+
|0.00 |     1.48   |  0.00    |  azzero  |
+-----+------------+----------+----------+


MPI and pyhton overhead
__________________________________

In this section we measure the overhead
for communication in the MPI version
and the one introduced by the python
interface. The simulation has been perfomed
wit 108 atoms and 10000 steps of size 5
as before (with flags -O3 -pg).



1. Total time serial version: 1.536s
2. Total time serial version with python interface: 3.032s
3. Total time parallel version with 1 process: 1.595s
4. Total time parallel version with 2 processes: 2.346s
5. Total time parallel version with 1 process with python interface: 3.004s
6. Total time parallel version with 2 processes with python interface: 3.484s


MPI benchmark
=============================

.. |fig2| image:: MPI_benchmark_2.png
   :scale: 100%
   :align: top

+---------+
| |fig2|  |
+---------+


