/***

    c) compute kinetic energy for given velocities and mass

 ***/


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "../include/others.h"



int main(){

  mdsys_t sys;
  
  
  /* allocate memory */
  sys.vx=(double *)malloc(sizeof(double));
  sys.vy=(double *)malloc(sizeof(double));
  sys.vz=(double *)malloc(sizeof(double));

  /*** Setting test parameters ***/
  int mass_t = 200;
  int atoms_t = 1;
  double vx_t = 0.;
  double vy_t = 1.;
  double vz_t = 1.;

  const double mvsq2e=2390.05736153349;
  
  int test_var = 1;
  
  /*** Setting initial conditions ***/
  sys.mass=mass_t;
  sys.natoms=atoms_t;
  
  sys.vx[0]=vx_t;
  sys.vy[0]=vy_t;
  sys.vz[0]=vz_t;
  
  sys.ekin=0.0;
  sys.temp=0.0;
  
  ekin(&sys);

  if(fabs(sys.ekin-0.5*mvsq2e*mass_t*(vx_t*vx_t + vy_t*vy_t + vz_t*vz_t))>10e-16)
    test_var = 0;
  
  
  if(test_var == 1)
    printf("\n\tTEST c) PASSED \n\n");
  else
    printf("\n\tTEST c) FAILED \n\n");

  /* deallocate memory */
  free(sys.vx);
  free(sys.vy);
  free(sys.vz);

  return 0;
}
