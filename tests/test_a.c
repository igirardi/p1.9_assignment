/***

    a) compute force for a few particle systems with atoms inside/outside the cutoff

 ***/


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "../include/others.h"
#include "../include/constants.h"
#include "../include/force.h"

/* helper function: apply minimum image convention */
static double pbc(double x, const double boxby2)
{
  while (x >  boxby2) x -= 2.0*boxby2;
  while (x < -boxby2) x += 2.0*boxby2;
  return x;
}


int main(){

  mdsys_t sys;
  double r, ffac, rx, ry, rz;
  int i, j, test_var = 0;
  double *test_fx, *test_fy, *test_fz, test_epot;
  
  /* setting basic parameters */
  sys.natoms = 4;          // number of atoms
  sys.mass = 39.948;       // mass in AMU for the Argon
  sys.epsilon = 0.2379;    // epsilon in kcal/mol
  sys.sigma = 3.405;       // sigma in angstrom
  sys.rcut = 8.5;          // rcut in angstrom
  sys.box = 17.1580;       // box length (in angstrom)
  
  /* allocate memory */
  sys.rx  = (double *) malloc(sys.natoms * sizeof(double));
  sys.ry  = (double *) malloc(sys.natoms * sizeof(double));
  sys.rz  = (double *) malloc(sys.natoms * sizeof(double));
  sys.vx  = (double *) malloc(sys.natoms * sizeof(double));
  sys.vy  = (double *) malloc(sys.natoms * sizeof(double));
  sys.vz  = (double *) malloc(sys.natoms * sizeof(double));
  sys.fx  = (double *) malloc(sys.natoms * sizeof(double));
  sys.fy  = (double *) malloc(sys.natoms * sizeof(double));
  sys.fz  = (double *) malloc(sys.natoms * sizeof(double));
  test_fx = (double *) malloc(sys.natoms * sizeof(double));
  test_fy = (double *) malloc(sys.natoms * sizeof(double));
  test_fz = (double *) malloc(sys.natoms * sizeof(double));

  /* setting initial condition */
  /* NEED TO PUT SOME COMMENTS TO EXPLAIN THE INITIALIZATION...*/
  sys.rx[0] = 6.67103294321331;
  sys.ry[0] = -10.6146871435653;
  sys.rz[0] = 12.6336939877734;
  sys.rx[1] = 6.67103294321331 + 0.25 * sys.rcut;
  sys.ry[1] = -10.6146871435653 + 0.25 * sys.rcut;
  sys.rz[1] = 12.6336939877734 + 0.25 * sys.rcut;
  sys.rx[2] = 6.67103294321331 - 0.3 * sys.rcut;
  sys.ry[2] = -10.6146871435653 - 0.3 * sys.rcut;
  sys.rz[2] = 12.6336939877734 - 0.3 * sys.rcut;
  sys.rx[3] = 6.67103294321331 - 6 * sys.rcut;
  sys.ry[3] = -10.6146871435653 - 6 * sys.rcut;
  sys.rz[3] = 12.6336939877734 - 6 * sys.rcut;

  sys.vx[0] = -1.5643224621482283e-03;
  sys.vy[0] = 4.8497508563925346e-04;
  sys.vz[0] = -4.3352481732883966e-04;
  sys.vx[1] = 4.1676710257651452e-04;
  sys.vy[1] = 2.2858522230176587e-05;
  sys.vz[1] = -6.1985040462745732e-04;
  sys.vx[2] = -7.5611349562333923e-04;
  sys.vy[2] = 4.0710138209103827e-04;
  sys.vz[2] = -4.6520198934056357e-04;
  sys.vx[3] = -1.0716463354369357e-03;
  sys.vy[3] = 1.9399472344407333e-03;
  sys.vz[3] = 3.6805207892014988e-05;
  
  sys.epot = 0.;
  test_epot = 0.;
  azzero(sys.fx, sys.natoms);
  azzero(sys.fy, sys.natoms);
  azzero(sys.fz, sys.natoms);
  azzero(test_fx, sys.natoms);
  azzero(test_fy, sys.natoms);
  azzero(test_fz, sys.natoms);

  /* testing force() function */
  for(i=0; i < (sys.natoms); ++i) {
    for(j=0; j < (sys.natoms); ++j) {
      
      /* particles have no interactions with themselves */
      if (i==j) continue;
      
      /* get distance between particle i and j */
      rx=pbc(sys.rx[i] - sys.rx[j], 0.5*sys.box);
      ry=pbc(sys.ry[i] - sys.ry[j], 0.5*sys.box);
      rz=pbc(sys.rz[i] - sys.rz[j], 0.5*sys.box);
      r = sqrt(rx * rx + ry * ry + rz * rz);
      
      /* compute force and energy if within cutoff */
      if (r < sys.rcut) {
	ffac = -4.0 * sys.epsilon * (-12.0 * pow(sys.sigma / r, 12.0) / r /r
				     + 6 * pow(sys.sigma / r, 6.0) / r / r);
	
	test_epot += 0.5 * 4.0 * sys.epsilon * (pow(sys.sigma / r, 12.0)
					       - pow(sys.sigma / r, 6.0));
	
	test_fx[i] += rx * ffac;
	test_fy[i] += ry * ffac;
	test_fz[i] += rz * ffac;
      }
    }
  }
  
  force(&sys);

  /* check errors */
  /***
      
      In the computation of the force there might be 
      round-off, differences errors. It is better 
      fabs(x - y) > precision instead of x != y.
      
   ***/
  for(i = 0; i < sys.natoms; ++i){
    if((test_fx[i] - sys.fx[i])>10.e-14)
      test_var++;
    if(fabs(test_fy[i] - sys.fy[i])>10.e-14)
      test_var++;
    if(fabs(test_fz[i] - sys.fz[i])>10.e-14)
      test_var++;
  }

  if(test_epot != sys.epot)
    test_var++;
  
  /* printing result */
  
  if(test_var)
    printf("\n\tTEST a) FAILED;\n\tNumber of errors: %d \n\n", test_var);
  else
    printf("\n\tTEST a) PASSED \n\n");
  
  /* deallocate memory */
  free(sys.rx);
  free(sys.ry);
  free(sys.rz);
  free(sys.vx);
  free(sys.vy);
  free(sys.vz);
  free(sys.fx);
  free(sys.fy);
  free(sys.fz);
  free(test_fx);
  free(test_fy);
  free(test_fz);

  return 0;
}
