/***

    b) compute one full step time integration for given forces and velocities (no call to force())

 ***/


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "../include/first_vel.h"
#include "../include/sec_vel.h"


int main(){

  mdsys_t sys;
  
  
  /* allocate memory */
  sys.rx=(double *)malloc(sizeof(double));
  sys.ry=(double *)malloc(sizeof(double));
  sys.rz=(double *)malloc(sizeof(double));
  sys.vx=(double *)malloc(sizeof(double));
  sys.vy=(double *)malloc(sizeof(double));
  sys.vz=(double *)malloc(sizeof(double));
  sys.fx=(double *)malloc(sizeof(double));
  sys.fy=(double *)malloc(sizeof(double));
  sys.fz=(double *)malloc(sizeof(double));

  /*** Setting test parameters ***/
  int mass_t = 200;
  int atoms_t = 1;
  double dt_t = 0.1;
  double rx_t = 1.;
  double ry_t = 1.;
  double rz_t = 1.;
  double vx_t = 0.;
  double vy_t = 1.;
  double vz_t = 1.;
  double fx_t = 300.;
  double fy_t = -320.;
  double fz_t = 49.;

  const double mvsq2e=2390.05736153349;
  
  int test_var = 1;
  
  /*** Setting initial conditions ***/
  sys.mass=mass_t;
  sys.natoms=atoms_t;
  sys.dt=dt_t;
  sys.rx[0]=rx_t;
  sys.ry[0]=ry_t;
  sys.rz[0]=rz_t;
  sys.vx[0]=vx_t;
  sys.vy[0]=vy_t;
  sys.vz[0]=vz_t;
  sys.fx[0]=fx_t;
  sys.fy[0]=fy_t;
  sys.fz[0]=fz_t;

  
  first_vel(&sys);
  

  if(sys.mass != mass_t)
    test_var = 0;
  if(sys.natoms != atoms_t)
    test_var = 0;
  if(sys.dt != dt_t)
    test_var = 0;
  
  if(fabs(sys.vx[0]-vx_t - 0.5*dt_t / mvsq2e * fx_t / mass_t)>10e-16)
    test_var = 0;
  if(fabs(sys.vy[0]-vy_t - 0.5*dt_t / mvsq2e * fy_t / mass_t)>10e-16)
    test_var = 0;
  if(fabs(sys.vz[0]-vz_t - 0.5*dt_t / mvsq2e * fz_t / mass_t)>10e-16)
    test_var = 0;
  if(fabs(sys.rx[0]-rx_t - dt_t*(vx_t + 0.5*dt_t / mvsq2e * fx_t / mass_t))>10e-16)
    test_var = 0;
  if(fabs(sys.ry[0]-ry_t - dt_t*(vy_t + 0.5*dt_t / mvsq2e * fy_t / mass_t))>10e-16)
    test_var = 0;
  if(fabs(sys.rz[0]-rz_t - dt_t*(vz_t + 0.5*dt_t / mvsq2e * fz_t / mass_t))>10e-16)
    test_var = 0;



  sec_vel(&sys);




  if(fabs(sys.vx[0]-vx_t - 0.5*dt_t / mvsq2e * fx_t / mass_t-0.5*dt_t / mvsq2e * fx_t / mass_t)>10e-16)
    test_var = 0;
  if(fabs(sys.vy[0]-vy_t - 0.5*dt_t / mvsq2e * fy_t / mass_t-0.5*dt_t / mvsq2e * fy_t / mass_t)>10e-16)
    test_var = 0;
  if(fabs(sys.vz[0]-vz_t - 0.5*dt_t / mvsq2e * fz_t / mass_t-0.5*dt_t / mvsq2e * fz_t / mass_t)>10e-16)
    test_var = 0;
  

  
  
  if(test_var == 1)
    printf("\n\tTEST b) PASSED \n\n");
  else
    printf("\n\tTEST b) FAILED \n\n");

  /* deallocate memory */
  free(sys.rx);
  free(sys.ry);
  free(sys.rz);
  free(sys.vx);
  free(sys.vy);
  free(sys.vz);
  free(sys.fx);
  free(sys.fy);
  free(sys.fz);

  return 0;  
}
