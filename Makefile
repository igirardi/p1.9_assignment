# -*- Makefile -*-
SHELL=/bin/sh
############################################
# derived makefile variables
OBJ_SERIAL=$(SRC:src/%.f90=Obj-serial/%.o)
############################################

default: exec

exec:
	$(MAKE) $(MFLAGS) -C Obj-$@

serial:
	$(MAKE) $(MFLAGS) -C Obj-$@

parallel:
	$(MAKE) -C Obj-exec parallel

scaling:
	$(MAKE) -C Obj-exec scaling

clean:
	$(MAKE) $(MFLAGS) -C Obj-serial clean
	$(MAKE) $(MFLAGS) -C Obj-exec clean
	$(MAKE) -C tests clean

test: test_a test_b test_c test_d
	./tests/test_a.x
	./tests/test_b.x
	./tests/test_c.x
	./tests/test_d.x

test_a: test_a
	$(MAKE) -C tests

test_b: test_b
	$(MAKE) -C tests

test_c: test_c
	$(MAKE) -C tests

test_d: test_d
	$(MAKE) -C tests
