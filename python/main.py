#!/usr/bin/env python

"""
Lennard-Jones Potential MD code with velocity verlet.
Units: Length=Angstrom, Mass=amu; Energy=kcal

Base python wrapper.

"""


from mpi4py import MPI
from ctypes import *
import unittest

# import DSO
dso = CDLL("./ljmd.so")

comm = MPI.COMM_WORLD
ncpus = comm.Get_size()
myrank = comm.Get_rank()

if myrank==0:
    print("Number of processes = ", ncpus)


########################### begin test ############################

class mydsys_t(Structure):
    _fields_=[("natoms",c_int),("nfi",c_int),("nsteps",c_int),("dt",c_double),("mass",c_double),("epsilon",c_double),("sigma",c_double),("box",c_double),("rcut",c_double),("ekin",c_double),("epot",c_double),("temp",c_double),("rx",POINTER(c_double)),("ry",POINTER(c_double)),("rz",POINTER(c_double)),("vx",POINTER(c_double)),("vy",POINTER(c_double)),("vz",POINTER(c_double)),("fx",POINTER(c_double)),("fy",POINTER(c_double)),("fz",POINTER(c_double))]



class time_integration_Test(unittest.TestCase):
    def test(self):
        rx_in = 1.0
        ry_in = 1.0
        rz_in = 1.0

        vx_in = 1.0
        vy_in = 2.0
        vz_in = 3.0

        fx_in = 3.0
        fy_in = 2.0
        fz_in = 1.0
        
        rx_s_p = (c_double*1)()
        rx_s_p[0]=rx_in
        ry_s_p = (c_double*1)()
        ry_s_p[0]=ry_in
        rz_s_p = (c_double*1)()
        rz_s_p[0]=rz_in
        
        vx_s_p = (c_double*1)()
        vx_s_p[0]=vx_in
        vy_s_p = (c_double*1)()
        vy_s_p[0]=vy_in
        vz_s_p = (c_double*1)()
        vz_s_p[0]=vz_in

        fx_s_p = (c_double*1)()
        fx_s_p[0]=fx_in
        fy_s_p = (c_double*1)()
        fy_s_p[0]=fy_in
        fz_s_p = (c_double*1)()
        fz_s_p[0]=fz_in

        dt_in=2.0
        mvsq2e=2390.05736153349
        mass_in=2.0

        p = mydsys_t(natoms=1,nfi=1,nsteps=1,dt=dt_in,mass=mass_in,epsilon=1.2,sigma=0.3,box=2.3,rcut=3.0,ekin=1.0,epot=1.0,temp=1.0,rx=rx_s_p,ry=ry_s_p,rz=rz_s_p,vx=vx_s_p,vy=vy_s_p,vz=vz_s_p,fx=fx_s_p,fy=fy_s_p,fz=fz_s_p)
        # dso.force(byref(p))
        # passed by reference implies a deep copy
        # passed by value implies a shallow copy and with
        # pointers is a very bad idea because it can happens
        # to do free(pointer) multiple times
       

        dso.first_vel(byref(p))
        
        self.assertEqual(p.vx[0], vx_in+0.5*dt_in/mvsq2e * fx_in / mass_in)
        self.assertEqual(p.vy[0], vy_in+0.5*dt_in/mvsq2e * fy_in / mass_in)
        self.assertEqual(p.vz[0], vz_in+0.5*dt_in/mvsq2e * fz_in / mass_in)
        self.assertEqual(p.rx[0], rx_in + dt_in*(vx_in + 0.5*dt_in/mvsq2e * fx_in / mass_in))
        self.assertEqual(p.ry[0], ry_in + dt_in*(vy_in + 0.5*dt_in/mvsq2e * fy_in / mass_in))
        self.assertEqual(p.rz[0], rz_in + dt_in*(vz_in + 0.5*dt_in/mvsq2e * fz_in / mass_in))


        dso.sec_vel(byref(p))

        self.assertEqual(p.vx[0], vx_in+0.5*dt_in/mvsq2e * fx_in / mass_in + 0.5*dt_in/mvsq2e*fx_in/mass_in)
        self.assertEqual(p.vy[0], vy_in+0.5*dt_in/mvsq2e * fy_in / mass_in + 0.5*dt_in/mvsq2e*fy_in/mass_in)
        self.assertEqual(p.vz[0], vz_in+0.5*dt_in/mvsq2e * fz_in / mass_in + 0.5*dt_in/mvsq2e*fz_in/mass_in)
        



########################### end test ##############################


if __name__ == "__main__":  

    f = open('argon_108.inp', 'r')

    list_data=[]
    for line in f:
        list_data.append(line.split(' ')[0])
    
    print(list_data)

    natoms = int(list_data[0])
    mass = float(list_data[1])
    epsilon = float(list_data[2])
    sigma = float(list_data[3])
    rcut = float(list_data[4])
    box = float(list_data[5])
    nsteps = int(list_data[9])
    dt = float(list_data[10])
    nprint = int(list_data[11])

    print(natoms, mass, epsilon, rcut, box, nsteps, dt, nprint)


    buf_rest = create_string_buffer(list_data[6].encode('utf-8'))
    buf_xyz = create_string_buffer(list_data[7].encode('utf-8'))
    buf_dat = create_string_buffer(list_data[8].encode('utf-8'))


    dso.func_main.argtypes=[c_int,c_double,c_double,c_double,c_double,c_double,c_int,c_double,c_int,c_char_p,c_char_p,c_char_p]
    dso.func_main(natoms,mass,epsilon,sigma,rcut,box,nsteps,dt,nprint,buf_rest,buf_xyz,buf_dat)

    print("Done")


   
