#ifndef __MY_STRUCT
#define __MY_STRUCT

#ifdef __MPI

#include <mpi.h>

struct _mdsys {
  int natoms,nfi,nsteps;
  double dt, mass, epsilon, sigma, box, rcut;
  double ekin, epot, temp;
  double *rx, *ry, *rz;
  double *vx, *vy, *vz;
  double *fx, *fy, *fz;

  /* MPI variables */
  int MyID, NPE, my_atoms, rest, offset;
  MPI_Comm mpicomm;
};
typedef struct _mdsys mdsys_t;

#else /* __MPI */

struct _mdsys {
  int natoms,nfi,nsteps;
  double dt, mass, epsilon, sigma, box, rcut;
  double ekin, epot, temp;
  double *rx, *ry, *rz;
  double *vx, *vy, *vz;
  double *fx, *fy, *fz;
};
typedef struct _mdsys mdsys_t;

#endif /* __MPI */

#endif /* __MY_STRUCT */

