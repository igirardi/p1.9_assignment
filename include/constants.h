#ifndef __CONSTANTS
#define __CONSTANTS

#define BLEN 200

static double kboltz=0.0019872067;     /* boltzman constant in kcal/mol/K */
static double mvsq2e=2390.05736153349; /* m*v^2 in kcal/mol */

#endif /* __CONSTANTS */
