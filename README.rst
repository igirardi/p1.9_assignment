N-Body LJMD Refactoring, Optimization and Parallelization Project
###################################################################

This repository has been created for the project
assigned for the final exams of the P1.9 course
of the master MHPC 2015. The name of the contributors
is given below:

igirardi **Ivan Girardi**

pdicerbo **Pierluigi Di Cerbo**

mowaisarain **Muhammad Owais**

The current version of the sofwtare is realese version 1.

In the current folder two version of the software
have been implemented, the default one is serial
(**make serial** or just **make**) and a parallel one
(**make parallel**). Test suite has been provide,
run **make test** to be sure that the version does not contain
bugs. In the folder *python* we provide a version with a
python interface. In that folder **make test** for running
the test suite and **make** or **make parallel** for
building the required libraries for the two
implementations. The input files .rest and .inp
of the python verison of the software have been
in the folder python (be sure to change the corresponding
name of the .inp file in the main.py source code).


Please let us know about the presence of bugs
or problems of the current release, we will manage
to solve them.

Contacts:

igirardi@sissa.it

pdicerbo@sissa.it

owaisarain@gmail.com


