#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "../include/first_vel.h"
#include "../include/constants.h"

/* velocity verlet part one: propagate velocities */
void first_vel(mdsys_t *sys){
  int i;

  /* const double mvsq2e=2390.05736153349; /\* m*v^2 in kcal/mol *\/ */

  /* first part: propagate velocities by half and positions by full step */
  for (i=0; i<sys->natoms; ++i) {
    sys->vx[i] += 0.5*sys->dt / mvsq2e * sys->fx[i] / sys->mass;
    sys->vy[i] += 0.5*sys->dt / mvsq2e * sys->fy[i] / sys->mass;
    sys->vz[i] += 0.5*sys->dt / mvsq2e * sys->fz[i] / sys->mass;
    sys->rx[i] += sys->dt*sys->vx[i];
    sys->ry[i] += sys->dt*sys->vy[i];
    sys->rz[i] += sys->dt*sys->vz[i];
  }
}
