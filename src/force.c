#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "../include/force.h"
#include "../include/others.h"
#include "../include/mystruct.h"
#include "../include/constants.h"

#ifdef __MPI
#include <mpi.h>
#endif /* __MPI */

/* helper function: apply minimum image convention */
/* static double pbc(double x, const double boxby2) */
double pbc(double x, const double boxby2)
{
  while (x >  boxby2) x -= 2.0*boxby2;
  while (x < -boxby2) x += 2.0*boxby2;
  return x;
}

/* compute forces */
void force(mdsys_t *sys) 
{
  double r,ffac;
  double rx,ry,rz;
  int i,j;

  double c12, c6, rsq, rcsq;
  double r6, rinv;
  
  c12=4.0*sys->epsilon*pow(sys->sigma,12);
    
  c6=4.0*sys->epsilon*pow(sys->sigma,6);

  rcsq = sys->rcut * sys->rcut;
  
#ifdef __MPI
  int i_par;
  double *tmp_fx, *tmp_fy, *tmp_fz, tmp_epot = 0.;
  double *Fx, *Fy, *Fz, *Rx, *Ry, *Rz, Epot = 0;
  /* int *revcounts, *displs, blocksize; */
  
  Fx = sys -> fx;
  Fy = sys -> fy;
  Fz = sys -> fz;
  Rx = sys -> rx;
  Ry = sys -> ry;
  Rz = sys -> rz;
#endif /* __MPI */
  
  /* zero energy and forces */
  sys->epot=0.0;
  azzero(sys->fx,sys->natoms);
  azzero(sys->fy,sys->natoms);
  azzero(sys->fz,sys->natoms);
  
#ifdef __MPI
  /* fprintf(stderr, "\tMyID = %d; in force()\n", sys -> MyID); */
  MPI_Bcast(Rx, sys -> natoms, MPI_DOUBLE, 0, sys -> mpicomm);
  MPI_Bcast(Ry, sys -> natoms, MPI_DOUBLE, 0, sys -> mpicomm);
  MPI_Bcast(Rz, sys -> natoms, MPI_DOUBLE, 0, sys -> mpicomm);    
  if(sys -> MyID == 0){
    tmp_fx = (double *)calloc(sys -> natoms, sizeof(double));
    tmp_fy = (double *)calloc(sys -> natoms, sizeof(double));
    tmp_fz = (double *)calloc(sys -> natoms, sizeof(double));

  }
#endif /* __MPI */

#ifdef __MPI
  
  for(i_par = 0; i_par < (sys -> my_atoms); ++i_par){
    i = i_par + (sys -> MyID) * (sys -> my_atoms) + sys -> offset;
#else
  for(i=0; i < (sys->natoms); ++i) {
#endif /* __MPI */
    
    for(j=i; j < (sys->natoms); ++j) {
      
      /* particles have no interactions with themselves */
      if (i==j) continue;
      rx=pbc(sys->rx[i] - sys->rx[j], 0.5*sys->box);
      ry=pbc(sys->ry[i] - sys->ry[j], 0.5*sys->box);
      rz=pbc(sys->rz[i] - sys->rz[j], 0.5*sys->box);
      rsq=rx*rx + ry*ry + rz*rz;
      
      /* compute force and energy if within cutoff */
      if (rsq < rcsq) {
	rinv = 1.0/rsq;
	r6 = rinv * rinv * rinv;
	
	ffac = (12.0*c12*r6 - 6.0*c6)*r6*rinv;
#ifndef __MPI	
	sys->epot += r6*(c12*r6 - c6);
	
	sys->fx[i] += rx*ffac;
	sys->fy[i] += ry*ffac;
	sys->fz[i] += rz*ffac;

	sys->fx[j] -= rx*ffac;
	sys->fy[j] -= ry*ffac;
	sys->fz[j] -= rz*ffac;
#else
	Epot += r6*(c12*r6 - c6);
	
	sys->fx[i] += rx*ffac;
	sys->fy[i] += ry*ffac;
	sys->fz[i] += rz*ffac;

	sys->fx[j] -= rx*ffac;
	sys->fy[j] -= ry*ffac;
	sys->fz[j] -= rz*ffac;

#endif /* __MPI */
      }
    }
  }
#ifdef __MPI
  
  MPI_Reduce(Fx, tmp_fx, sys -> natoms, MPI_DOUBLE, MPI_SUM, 0, sys -> mpicomm);
  MPI_Reduce(Fy, tmp_fy, sys -> natoms, MPI_DOUBLE, MPI_SUM, 0, sys -> mpicomm);
  MPI_Reduce(Fz, tmp_fz, sys -> natoms, MPI_DOUBLE, MPI_SUM, 0, sys -> mpicomm);
  MPI_Reduce(&Epot, &tmp_epot, 1, MPI_DOUBLE, MPI_SUM, 0, sys -> mpicomm);
  
  if(sys -> MyID == 0){
    for(j = 0; j < sys -> natoms; ++j){
      sys -> fx[j] = 0.;
      sys -> fy[j] = 0.;
      sys -> fz[j] = 0.;
      sys -> fx[j] = tmp_fx[j];
      sys -> fy[j] = tmp_fy[j];
      sys -> fz[j] = tmp_fz[j];
    }
    sys -> epot = tmp_epot;
  }
#endif /* __MPI */
}
