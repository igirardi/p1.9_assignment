/* 
 * Lennard-Jones Potential MD code with velocity verlet.
 * units: Length=Angstrom, Mass=amu; Energy=kcal
 *
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "../include/mystruct.h"
#include "../include/first_vel.h"
#include "../include/sec_vel.h"
#include "../include/force.h"
#include "../include/inout.h"
#include "../include/others.h"
#include "../include/constants.h"

#ifdef __MPI
#include <mpi.h>
#endif /* __MPI */

/* main */
void func_main(int natoms_py, double mass_py, double epsilon_py, double sigma_py, double rcut_py, double box_py, int nsteps_py, double dt_py, int nprint_py, char* buff_rest, char* buff_xyz, char* buff_dat)
{
    int nprint, i;
    char restfile[BLEN], trajfile[BLEN], ergfile[BLEN], line[BLEN];
    FILE *fp,*traj,*erg;
    mdsys_t sys;

#ifdef __MPI
    //MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &(sys.MyID));
    MPI_Comm_size(MPI_COMM_WORLD, &(sys.NPE));    
    sys.mpicomm = MPI_COMM_WORLD;
    /* fprintf(stderr, "\n\tSTART running process %d\n", sys.MyID); */
    int ntobcast, nsteptb;
    double mtobcast, etobcast, stobcast, rtobcast, btobcast;
    if(sys.MyID == 0){
#endif /* __MPI */

    sys.natoms=natoms_py;
    sys.mass=mass_py;
    sys.epsilon=epsilon_py;
    sys.sigma=sigma_py;
    sys.rcut=rcut_py;
    sys.box=box_py;
    sys.nsteps=nsteps_py;
    sys.dt=dt_py;
    nprint=nprint_py;

#ifdef __MPI
    ntobcast = sys.natoms;
    mtobcast = sys.mass;
    etobcast = sys.epsilon;
    stobcast = sys.sigma;
    rtobcast = sys.rcut;
    btobcast = sys.box;
    nsteptb  = sys.nsteps;
    }
    
    /* MPI_Bcast(&(sys.natoms), 1, MPI_INT, 0, MPI_COMM_WORLD); */
    MPI_Bcast(&ntobcast, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&mtobcast, 1, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(&etobcast, 1, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(&stobcast, 1, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(&rtobcast, 1, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(&btobcast, 1, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(&nsteptb,  1, MPI_DOUBLE, 0, sys.mpicomm);

    if(sys.MyID){
      sys.natoms = ntobcast;
      sys.mass = mtobcast;
      sys.epsilon = etobcast;
      sys.sigma = stobcast;
      sys.rcut = rtobcast;
      sys.box = btobcast;
      sys.nsteps = nsteptb;
    }
    sys.my_atoms = sys.natoms / sys.NPE;
    sys.rest = sys.natoms % sys.NPE;
    sys.offset = 0;
    
    if(sys.rest != 0 && sys.MyID < sys.rest)
      sys.my_atoms++;
    else
      sys.offset = sys.rest;
#endif
    
    /* allocate memory */
    sys.rx=(double *)malloc(sys.natoms*sizeof(double));
    sys.ry=(double *)malloc(sys.natoms*sizeof(double));
    sys.rz=(double *)malloc(sys.natoms*sizeof(double));
    sys.vx=(double *)malloc(sys.natoms*sizeof(double));
    sys.vy=(double *)malloc(sys.natoms*sizeof(double));
    sys.vz=(double *)malloc(sys.natoms*sizeof(double));
    sys.fx=(double *)malloc(sys.natoms*sizeof(double));
    sys.fy=(double *)malloc(sys.natoms*sizeof(double));
    sys.fz=(double *)malloc(sys.natoms*sizeof(double));

    /* read restart */

#ifdef __MPI
    /* only process 0 read the file */
    if(sys.MyID == 0){
#endif /* __MPI */

    fp=fopen(buff_rest,"r");
    if(fp) {
        for (i=0; i<sys.natoms; ++i) {
            fscanf(fp,"%lf%lf%lf",sys.rx+i, sys.ry+i, sys.rz+i);
        }
        for (i=0; i<sys.natoms; ++i) {
            fscanf(fp,"%lf%lf%lf",sys.vx+i, sys.vy+i, sys.vz+i);
        }
        fclose(fp);
        azzero(sys.fx, sys.natoms);
        azzero(sys.fy, sys.natoms);
        azzero(sys.fz, sys.natoms);
    } else {
        perror("cannot read restart file");

#ifdef __MPI
	MPI_Finalize();
#endif /* __MPI */
	
        return 3;
    }
    
#ifdef __MPI
    }
    MPI_Bcast(sys.rx, sys.natoms, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(sys.ry, sys.natoms, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(sys.rz, sys.natoms, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(sys.vx, sys.natoms, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(sys.vy, sys.natoms, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(sys.vz, sys.natoms, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(sys.fx, sys.natoms, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(sys.fy, sys.natoms, MPI_DOUBLE, 0, sys.mpicomm);
    MPI_Bcast(sys.fz, sys.natoms, MPI_DOUBLE, 0, sys.mpicomm);
#endif /* __MPI */

    /* initialize forces and energies.*/
    sys.nfi=0;
    force(&sys);
    ekin(&sys);

#ifdef __MPI
    if(sys.MyID == 0){
      erg=fopen(buff_dat,"w");
      traj=fopen(buff_xyz,"w");
      
      printf("Starting simulation with %d atoms for %d steps.\n",sys.natoms, sys.nsteps);
      printf("     NFI            TEMP            EKIN                 EPOT              ETOT\n");
      output(&sys, erg, traj);
    }
#else
    erg=fopen(buff_dat,"w");
    traj=fopen(buff_xyz,"w");

    printf("Starting simulation with %d atoms for %d steps.\n",sys.natoms, sys.nsteps);
    printf("     NFI            TEMP            EKIN                 EPOT              ETOT\n");
    output(&sys, erg, traj);
#endif /* __MPI */

    /**************************************************/
    /* main MD loop */
    for(sys.nfi=1; sys.nfi <= sys.nsteps; ++sys.nfi) {
      
      /* write output, if requested */
#ifdef __MPI
      /* fprintf(stderr, "\n\tMyID = %d; starting cycle with index %d\n", sys.MyID, sys.nfi); */
      /* MPI_Barrier(sys.mpicomm); */
      if(sys.MyID == 0)
	if ((sys.nfi % nprint) == 0)
	  output(&sys, erg, traj);
      
      /* propagate system and recompute energies */
      if(sys.MyID == 0)
	first_vel(&sys);
      
      /* MPI_Barrier(sys.mpicomm); */
      /* fprintf(stderr, "\n\tMyID = %d; entering in force()", sys.MyID); */
      force(&sys);
      
      if(sys.MyID == 0){
	sec_vel(&sys);
	
	ekin(&sys);
      }
#else
      /* write output, if requested */
      if ((sys.nfi % nprint) == 0)
	output(&sys, erg, traj);
      
      /* propagate system and recompute energies */
      
      first_vel(&sys);
      force(&sys);
      sec_vel(&sys);
      
      ekin(&sys);
#endif /* __MPI */
    }
    /**************************************************/
    
    /* clean up: close files, free memory */
#ifdef __MPI
    free(sys.rx);
    free(sys.ry);
    free(sys.rz);
    free(sys.vx);
    free(sys.vy);
    free(sys.vz);
    free(sys.fx);
    free(sys.fy);
    free(sys.fz);

    fprintf(stderr, "Simulation Done.%d\n", sys.MyID);

    if(sys.MyID == 0){
      fclose(erg);
      fclose(traj);
    }

    //MPI_Finalize();
#else
    printf("Simulation Done.\n");
    fclose(erg);
    fclose(traj);

    free(sys.rx);
    free(sys.ry);
    free(sys.rz);
    free(sys.vx);
    free(sys.vy);
    free(sys.vz);
    free(sys.fx);
    free(sys.fy);
    free(sys.fz);
#endif /* __MPI */

    return 0;
}
