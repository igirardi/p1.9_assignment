#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <math.h>
#include "../include/sec_vel.h"
#include "../include/constants.h"

void sec_vel(mdsys_t *sys){
  int i;

  /* const double mvsq2e=2390.05736153349; /\* m*v^2 in kcal/mol *\/ */

  /* second part: propagate velocities by another half step */
  for (i=0; i<sys->natoms; ++i) {
    sys->vx[i] += 0.5*sys->dt / mvsq2e * sys->fx[i] / sys->mass;
    sys->vy[i] += 0.5*sys->dt / mvsq2e * sys->fy[i] / sys->mass;
    sys->vz[i] += 0.5*sys->dt / mvsq2e * sys->fz[i] / sys->mass;
  }
}

