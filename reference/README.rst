Reference files
==============================


These files are reference energy output form running serial
and parallel version for 1000 steps. For a larger 
number of steps, e.g. 10000, the difference of the files
is not zero because of round-off and approximation
errors. We have observed this behavior to happen
in the MPI version of the code if the loop
over j is performed form i to natoms
and not from 0 to natoms.

